<?php

use App\Http\Controllers\PagesController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/hello', function () {
//     //return view('welcome');
//     return 'Hello World';
// });

// Route::get('/users/{id}/{name}', function ($id, $name) {
//     return 'This is user '.$name. ' with an id of '.$id;
// });

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/', 'PagesController@index') ;

// Route::get('/about', function () {
//     return view('pages.about');
// });

Route::get('/', 'App\Http\Controllers\PagesController@index');
Route::get('/about', 'App\Http\Controllers\PagesController@about');
Route::get('/services', 'App\Http\Controllers\PagesController@services');


// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Route::post('/posts', 'App\Http\Controllers\PostsController');
//Route::resource('/posts', 'App\Http\Controllers\PostsController');
//Route::post('posts', 'App\Http\Controllers\PostsController');
Route::resource('posts', 'App\Http\Controllers\PostsController'); 
// Route::patch('posts/{id}', 'App\Http\Controllers\PostsController');


// Auth::routes();

// Route::get('home', [App\Http\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', 'App\Http\HomeController@index');
//Route::get('/home', [HomeController::class, 'index']);